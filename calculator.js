﻿function count() {
    var fvalue = document.getElementById('first_value').value;
    var lvalue = document.getElementById('second_value').value;
    var operation = document.getElementById('operation').value;

    switch (operation) {
        case '+':
            var html = parseInt(fvalue) + parseInt(lvalue);
            break;
        case '-':
            var html = parseInt(fvalue) - parseInt(lvalue);
            break;
        case '*':
            var html = parseInt(fvalue) * parseInt(lvalue);
            break;
        case '/':
            var html = parseInt(fvalue) / parseInt(lvalue);
            break;
        default:
            var html = "There is no such an operation";
            break;
    }
    
    document.getElementById('result').innerHTML = html;
}

document.getElementById('count').addEventListener('click', count);